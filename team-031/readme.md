# EDOM - Projecto de Tópico de Pesquisa

---

O presente documento foi desenvolvido no âmbito da unidade curricular de Engenharia de Domínio (EDOM),
no primeiro ano do Mestrado em Engenharia Informática (MEI), no ramo de Engenharia de Software, no Instituto Superior 
de Engenharia do Porto (ISEP).


O projeto foi desenvolvido por: 
* Daniel Dias - 1130182
* João Almeida - 1190107 
* João Vieira - 1190112
* Luís Pereira - 1160994


# Viatra

O VIATRA é uma framework open-souce de transformação de modelos, com foco na avaliação eficiente de consultas de modelo e suporta vários fluxos de trabalho de transformação.

O VIATRA suporta o desenvolvimento de transformações de modelo com foco específico em transformações reactivas acionadas por eventos. Com base no suporte incremental à consulta, o VIATRA oferece uma linguagem para definir transformações e um mecanismo de transformação reactivo para executar determinadas transformações mediante alterações no modelo subjacente.

O projeto VIATRA fornece:

- Um mecanismo de consulta incremental junto com uma linguagem baseada num padrão de grafo para especificar e executar consultas de modelo com eficiência.
- Uma DSL interna sobre a linguagem Xtend para especificar transformações reactivas acionadas por batch e por eventos.
- Uma estrutura de exploração do espaço de design baseada em regras para explorar candidatos a design como modelos que atendem a vários critérios. 
- Um ofuscador de modelo para remover informações confidenciais de um modelo confidencial (por exemplo, para criar relatórios de erros).

O actual projeto VIATRA foi rescrito completamente apartir da framework VIATRA2, com total compatibilidade e suporte aos modelos EMF.

## Como usar

O Viatra pode ser instalado apartir de "Install New Software" no menu Help.

![alt text](eclipse_install.png "Instalação no Eclipse")

*Figura 1 - Instalação no Eclipse*

As restrições e condições na estrutura do VIATRA são expressas usando uma linguagem baseada em padrões grafos. Este formalismo declarativo permite definições muito compactas de condições complexas, enquanto ainda é possível fornecer avaliação de querys com base no algoritmo Rete.

Um padrão grafo (graph pattern) codifica uma query nomeada com alguns parâmetros definidos como uma disjunção de padrões, em que cada padrão consiste num conjunto de restrições. O resultado de um padrão grafo, chamado um conjunto de correspondências, é um conjunto de tuples (elemento do modelo) em que os elementos atendem a todas as restrições definidanum dos corpos do padrão.

Para definir queries, primeiro um VIATRA Query Project deve ser criado com o New Project Wizard do Eclipse. Estes projetos são projetos com dependências pré-configuradas do VIATRA e o gerador de queries inicializado. As especificações da query devem ser adicionadas ao caminho de classe Java dos projetos, mais especificamente nos pacotes Java. Com base nessas observações, a criação da primeira query consiste nas seguintes etapas:

1. Criar um novo projeto VIATRA Query no host Eclipse com qualquer nome, por exemplo org.eclipse.viatra.examples.cps.queries.

2. Incluir org.eclipse.viatra.examples.cps.model nas dependências do Plug-in para disponibilizar o metamodelo do CPS para o projeto.

3. Criar um Java package para armazenar as consultas, por exemplo org.eclipse.viatra.examples.cps.queries.

4. Criar uma nova query definition num pacote nomeado usando o New Query Definition wizard da categoria VIATRA, por exemplo CPSQueries.vql.

5. Preencher a query:

![alt text](First_Query.png "Query de Viatra")

*Figura 2 - Query de Viatra*


## Validação

O VIATRA fornece recursos para criar regras de validação com base na linguagem de padrões da framework. Estas regras podem ser avaliadas em vários modelos de instância EMF e se forem encontradas violações de constraints,  são criados automaticamente marcadores na Eclipse Problems View.

A anotação @Constraint pode ser usada para derivar uma regra de validação de um padrão grafo como uma regra de validação. O padrão da regra identifica os elementos errados do modelo, enquanto os parâmetros de anotação fornecem informações sobre como apresentar os resultados na interface com o utilizador. A anotação usa os seguintes parâmetros:

- key: a lista de parâmetros que determinam a quais objetos a violação da constraint deve ser anexada.

- message: a mensagem a ser exibida quando a violação da constraint for encontrada.

- severity: "warning" ou "error"

- targetEditorId: um ID do editor Eclipse em que a framework de validação deve registar-se no menu de contexto.

Na figura seguinte é possível ver um exemplo:

![alt text](ViatraValidation.png "Validação no Viatra")

*Figura 3 - Validação no Viatra*

Ao adicionar uma anotação de restrição (constraint) a um padrão grafo, o gerador de código inicializa um projeto de validação que inclui e registra as regras derivadas dos padrões para a framework. Estas regras podem ser testadas através de uma nova instância do Eclipse:

- Iniciar uma nova aplicação de Eclipse com todos os projetos no workspace.

- Nessa instância, é possível abrir um modelo de instância incorreto no editor de modelos (deve-se verificar se o editor é o mesmo que foi incluído no parâmetro targetEditorId).

- Iniciar a validação no menu pop-up clicando em "VIATRA Validation".


## Conceitos de Modelagem no VIATRA

Uma abordagem de verificação baseada em transformação de modelos para modelos UML requer a definição precisa de modelos de vários application domains, que são especificados uniformemente pelas técnicas de metamodelo visuais no VIATRA. Um método preciso de metamodelo inclui (pelo menos) a definição formal da sintaxe abstrata, a semântica estática e dinâmica de uma linguagem. 

No VIATRA, a sintaxe estática (static syntax) de uma linguagem de modelagem é especificada através de diagramas de classes UML e formalizada por grafos digitados, atribuídos e direcionados. Os metamodelos são interpretados como grafos de tipos (type) e os modelos são instâncias válidas dos grafos de tipos. 

Portanto, no VIATRA, as técnicas de metamodelo não lidam apenas com a definição de perfis UML, mas também especificam a notação e a estrutura dos modelos matemáticos. Nas abordagens tradicionais de metamodelação, a semântica estática de uma linguagem de modelagem é definida através do OCL (Object Constraint Language). No entanto, a OCL tem várias desvantagens do ponto de vista da engenharia, pois é puramente textual (em contraste com os diagramas visuais de classe UML) e não é declarativa (é uma linguagem de programação e não uma linguagem de especificação). 

O VIATRA usa uma técnica de descrição declarativa e baseada em padrões para semântica estática, onde a maioria das restrições de formatação são fornecidas por padrões grafos (graph patterns), que têm uma vantagem sobre a OCL que é o facto de preservar visualidade. Os padrões grafos também desempenham um papel importante na definição da semântica dinâmica de um modelo, pois a evolução de um modelo é descrita pelas regras de transformação de grafos. As transformações within e between dos modelos são especificadas uniformemente pelas regras de transformação de grafos correspondentes, fornecendo assim uma maneira visual de entender definições semânticas.

Na Figura 4 está representado um metamodelo para demonstrar os conceitos de metamodelo do VIATRA. 

![alt text](MetamodeloAutomnFinito.png "Metamodelo de Automato Finito")

*Figura 4 - Metamodelo de Autómato Finito*

O metamodelo é composto por:

- Nós: (i) MetaClasses como Autómatos ou Transição; (ii) MetaAttributes como nome ou char; (iii) tipos básicos, como String, por exemplo;

- Arestas: (i) relações de agregação (associações) entre MetaClasses, por exemplo um autómato é composto por estados, transições de estado e links de transição; (ii) Relações de referência, que definem, por exemplo, que uma Transição está relacionada a *a* partir de um Estado e *a* a um Estado pelas associações correspondentes; (iii) Relações de herança: um InitState é um Estado, portanto, pode ser conectado a outros estados através de transições;

A relação de referência actual é um conceito dinâmico no metamodelo, pois poderá ser modificado pelas regras de transformação posteriormente, enquanto que os componentes estáticos nunca são alterados após serem criados.

### Modelos

Em geral, modelos são instâncias de metamodelos, ou seja, são instanciados num metalevel inferior. Por exemplo, a Figura 5 (a) é uma instância válida do metamodelo da Figura 4. Isso significa que uma instância da MetaClasse *Estado* é um estado concreto *s1* numa instância específica *a1* de um autómato. Além disso, uma instância *e1* de uma relação de metamodelo (por exemplo, Estado) é um link concreto entre os objetos anteriores.

O VIATRA, formaliza os conceitos de modelos e metamodelos tratando-os como grafos direcionados, digitados e atribuídos (directed, typed and attributed). Uma borda *e1* (do tipo Estado) pode levar entre *a1* (do tipo Autómato) e *s1* (do tipo Estado) se existir um nó (node) Autómato no metamodelo, em que *e1* ou uma de suas superclasses esteja conectada a um nó *Estado* (ou ao uma superclasse de Estado).

![alt text](ModeloAtomatoFinito.png "Modelo de Automato Finito")

*Figura 5 - Modelo de Autómato Finito*

### Definir Semânticas Dinâmicas

As partes semânticas do metamodelo são estabelecidas principalmente por aspectos estáticos (static aspects) e aspectos dinâmicos. No VIATRA, ambas as partes da semântica são baseadas nas técnicas de manipulação de padrões de transformação de grafos.

Uma regra de transformação de grafos é uma 3-tuple *Rule = (Lhs, N, Rhs)*, onde *Lhs* é *left-hand side graph*, *Rhs* é *right-hand side graph*, enquanto que *N* é um grafo de condição de aplicacão negativa (Negative application condition graph), sendo este último um parâmetro opcional. Um exemplo pode ser visto na figura 6.

A aplicação de uma regra a um grafo de modelo (modelo UML do utilizador) *M* reescreve o modelo do utilizador através da substituição do padrão definido por *Lhs* pelo padrão de *Rhs*. Isto é realizado por:

1. Encontrar uma ocorrência de *Lhs* no *M* (graph pattern matching);

2. Verificação das condições negativas de aplicação *N* que proíbem a presença de certos nós e arestas;

3. Remover uma parte do grafo *M* que pode ser mapeada para o grafo *Lhs*, mas não para o grafo *Rhs* (produzindo o grafo de contexto);

4. Colar *Rhs* e grafo de contexto, obtendo-se assim o modelo derivado *M'*;


![alt text](SemanticasDinamicasAutomatoFinito.png "Semânticas Dinâmicas do Autómato Finito")

*Figura 6 - Semânticas Dinâmicas do Autómato Finito*

Quando a regra da figura 6, que descreve a semântica operacional dinâmica de autómatos finitos, é aplicada ao grafo do modelo da figura 5 (a), então, inicialmente, essa transição do autómato deve possuir um *source state* activo, ou seja, que possui um estado conectado por uma borda *from* e marcado como *current*. Finalmente, é obtido um novo modelo de autómatos finitos (representado na figura 5 (b)), onde o estado activo é alterado para o estado de transição, conforme indicado pela aresta actual reescrita.

### Regras de Transformação de Modelos

Como o principal objetivo da transformação do modelo é derivar um modelo alvo de um determinado modelo de origem, os objetos de origem e de destino são vinculados para formar um único grafo. Por esse motivo, os conceitos dos grafos de referência são introduzidos. A estrutura de um grafo de referência também é restringida por um metamodelo de referência correspondente, que contém (i) referências de nós de metamodelos de origem e de destino existentes; (ii) novos nós de referência que fornecem um acoplamento digitado de objetos de origem e de destino e (iii) bordas de referência que interrelacionam nós. Os grafos de referência também fornecem a base principal para a anotação posterior dos resultados da análise. Toda a semântica operacional de um modelo VIATRA é definida por um sistema de transição de modelos, em que a próxima regra de transformação de um grafo a ser aplicada num modo específico é restringida por um grafo de fluxo de controlo. Como a maioria das regras executa modificações locais nos modelos, elas podem ser executadas paralelamente para todas as ocorrências (forall mode). Como alternativa, uma regra é aplicada numa correspondência única (try mode) ou é aplicada o maior tempo possível (loop mode).

## Visão Tecnológica do VIATRA

Nesta secção é feita uma breve visão geral tecnológica da estrutura do VIATRA, descrevendo um cenário típico de transformação de modelo em um ambiente UML. O conceito tecnológico geral da estrutura é o uso do padrão XMI para metamodelos arbitrários baseados em MOF (incluindo simultaneamente UML e modelos matemáticos como redes de Petri, redes de fluxo de dados, autómatos hierárquicos etc.) para obter uma arquitetura aberta e independente de ferramenta.

**Fase de Design:** a interação com o VIATRA começa com uma fase de design:

1. Como etapa inicial, cria-se os metamodelos de MOF (Meta-Object Facility) da linguagem de modelagem de origem e destino. Posteriormente, relacionamos os objetos de origem e destino construindo-se o metamodelo de referência.

2. Exportar os metamodelos em formato XMI, em conformidade com o Modelo MOF. Estes arquivos servem como entradas primárias para o VIATRA.

3. No passo seguinte, as regras de transformação e as estruturas de controlo são criadas por um perfil UML especial (na forma de um Rational Rose Add-In) adaptado para sistemas de transformação de gráficos e exportadas para um formato UML XMI.

**Geração Automatizada do Programa:** na próxima fase, uma implementação Prolog do programa de transformação é gerada automaticamente.

1. Os metamodelos de origem, destino e referência são compilados numa representação Prolog interna. O processo de compilação é feito por um meta-metamodelo comum no qual os metamodelos do utilizador são validados.

2. Em segundo lugar, o programa de transformação é gerado na forma de um código Prolog. Este método automatizado de geração de programas, que é o núcleo semântico da estrutura do VIATRA, também foi projetado e implementado por transformações consecutivas de modelo de maneira reflexiva.

**Transformações Automatizadas:** finalmente, os programas de transformação gerados anteriormente podem ser aplicados à transformação de vários modelos de origem.

1. O modelo UML criado por um engenheiro de software servirá como entrada de uma transformação e, portanto, é exportado para um formato XMI.

2. O modelo de origem é compilado numa representação interna do Prolog. Enquanto que, a correção sintática do modelo de origem é validada em relação ao metamodelo de origem.

3. O modelo de origem é transformado pelo programa de transformação e dois modelos são gerados como saída: a referência e o modelo de destino (a conformidade entre os metamodelos é verificada para os dois modelos neste momento).

4. Finalmente, os modelos de saída são exportados em formato XMI. A linguagem de entrada concreta de uma ferramenta de análise específica geralmente pode ser gerada a partir deste formato XMI por (i) programas Prolog simples (ii) transformações XSLT ou (iii) programas Java.

## Limitações da OCL

Como o VIATRA é visto como uma alternativa à OCL, foi considerado importante apresentar uma série de limitações desta linguagem.

#### Feedback limitado do utilizador:
O OCL não suporta a especificação de mensagens significativas que podem ser relatadas ao utilizador caso uma invariante não seja satisfeita para certos elementos. 
O feedback é limitado ao nome da invariante e à(s) instância(s) para as quais ele falhou.

#### Não há suporte para avisos / críticas
Na OCL não existe tal distinção entre erros e avisos e em consequência, todos os problemas relatados são considerados erros. 
Isso adiciona uma carga adicional para identificar e priorizar questões de grande importância.

#### Não há suporte para restrições dependentes
Cada invariante da OCL é uma unidade independente que não depende de outras invariantes.
Todas as invariantes são tratadas como unicas, portanto, ao validar o meta-modelo, todas as invariantes são verificadas consumindo tempo desnecessário porque mesmo se um falhar, os outros são testados.

#### Flexibilidade limitada na definição de contexto
As invariantes OCL são definidas no contexto de meta-classes. 
Enquanto isso alcança um particionamento razoável do espaço do elemento do modelo, há casos em que é necessário um particionamento mais refinado.
Como a OCL suporta apenas o particionamento do espaço do elemento do modelo, meta-classes, invariantes devem aparecer sob o mesmo contexto (ou seja, Classe). Além disso, cada invariante deve explicitamente definir que ela aborda uam ou outra
sub-partição. 
Portanto, cada invariante deve limitar seu alcance inicialmente (usando o self.isA expressão) e depois expressar o corpo real.

#### Não há suporte para reparar inconsistências
Embora a OCL possa ser usada para detectar inconsistências, ela não fornece nenhum meio para reparar
essas mesmas inconsitências. 
A razão é que a OCL foi concebida como uma linguagem livre de efeitos colaterais e não possui construções para modificar modelos. 
No entanto, existem muitos casos em que as inconsistências são triviais para resolver e os utilizadores podem beneficiar de reparação semi-automática das instalações.

#### Não há suporte para restrições entre modelos
As expressões OCL (e, portanto, restrições OCL) só podem ser avaliadas no contexto de um modelo único de cada vez. 
Consequentemente, a OCL não pode ser usada para expressar restrições que em diferentes modelos. 
No contexto de um processo de engenharia conduzido por um modelo em grande escala que envolve muitos modelos diferentes (que potencialmente se adaptam a diferentes linguagens de
guages) esta limitação é particularmente severa.

# Referências

1. *Concepts of Model Verification and Validation*, disponível online em: https://inis.iaea.org/collection/NCLCollectionStore/_Public/36/030/36030870.pdf?r=1&r=1

2. *Viatra Main Page*, disponível online em: https://www.eclipse.org/viatra/

3. *Generic and Meta-transformations for Model Transformation Engineering*, disponível online em: https://www.researchgate.net/profile/Andras_Pataricza/publication/220868510_Generic_and_Meta-transformations_for_Model_Transformation_Engineering/links/0c960519dfed3901f8000000/Generic-and-Meta-transformations-for-Model-Transformation-Engineering.pdf

4. *Advanced Model Transformation Language Constructs in the VIATRA2 Framework*, disponível online em: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.74.8205&rep=rep1&type=pdf 

5. *Object Management Group*, disponível online em: https://www.omg.org/mof/

6. *VIATRA Solver: A Framework for the Automated Generation of Consistent Domain-Specific Models*, disponível online em: https://inf.mit.bme.hu/sites/default/files/publications/ICSE19Demo-VS.pdf

7. *Incremental Pattern Matching in the VIATRA Model Transformation System∗*, disponível online em: http://home.mit.bme.hu/~rath/pub/conf/gramot4-rath.pdf

8. *OCL Constraints Generation from Natural Language Specification*, disponível online em: https://ieeexplore.ieee.org/document/5630350